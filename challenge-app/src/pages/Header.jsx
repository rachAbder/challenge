import React from 'react'

function Header() {
    return (
        <>
            <header id="header">
                <div classNameName="container">
                    <nav id="navbar" classNameName="navbar">
                        <ul>
                            <li><a classNameName="nav-link active" href="#header">Home</a></li>
                            <li><a classNameName="nav-link" href="#about">About</a></li>
                            <li><a classNameName="nav-link" href="#resume">Resume</a></li>
                            <li><a classNameName="nav-link" href="#services">Services</a></li>
                            <li><a classNameName="nav-link" href="#portfolio">Portfolio</a></li>
                            <li><a classNameName="nav-link" href="#contact">Contact</a></li>
                        </ul>
                        <i className="bi bi-list mobile-nav-toggle"></i>
                    </nav>
                    <div className="social-links">
                        <a href="#" className="twitter"><i className="bi bi-twitter"></i></a>
                        <a href="#" className="facebook"><i className="bi bi-facebook"></i></a>
                        <a href="#" className="instagram"><i className="bi bi-instagram"></i></a>
                        <a href="#" className="linkedin"><i className="bi bi-linkedin"></i></a>
                    </div>
                </div>
            </header>
        </>
    )
}

export default Header